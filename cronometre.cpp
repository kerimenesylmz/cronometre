#include "cronometre.h"
#include <QDateTime>
#include <QDebug>
#define currentSecsSinceEpoch QDateTime::currentMSecsSinceEpoch() / 1000

Cronometre::Cronometre()
{
	stopped = true;
}

void Cronometre::start()
{
	stopped = false;
	t1 = currentSecsSinceEpoch;
	t2 = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "started";
}

void Cronometre::restart()
{
	start();
}

void Cronometre::stop()
{
	stopped = true;
	t1 = currentSecsSinceEpoch - t1;
	t2 = QDateTime::currentMSecsSinceEpoch() - t2;
	qDebug() << "stopped";
}

qint64 Cronometre::elapse()
{
	if (stopped)
		return t1;
	return currentSecsSinceEpoch - t1;
}

qint64 Cronometre::elapseMs()
{
	if (stopped)
		return t2;
	return QDateTime::currentMSecsSinceEpoch() - t2;
}

bool Cronometre::isRunning()
{
	if (!stopped)
		return true;
	return false;
}
