#include <QCoreApplication>
#include <cronometre.h>
#include "unistd.h"
#include <QDebug>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	Cronometre cron;
	cron.start();
	sleep(3);
	int k = 0;
	while (1) {
		qDebug() << cron.elapse();
		qDebug() << cron.elapseMs();
		sleep(1);
		if (k == 10)
			break;
		k++;
		qDebug() << "is running" << cron.isRunning();
	}
	cron.stop();
	qDebug() << cron.elapse();
	qDebug() << cron.elapseMs();
	qDebug() << "is running" << cron.isRunning();
	sleep(3);
	qDebug() << cron.elapse();
	qDebug() << cron.elapseMs();
	return a.exec();
}
