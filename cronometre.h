#ifndef CRONOMETRE_H
#define CRONOMETRE_H

#include <QtGlobal>

class Cronometre
{
public:
	Cronometre();
	void stop();
	void start();
	void restart();

	qint64 elapse();
	qint64 elapseMs();
	bool isRunning();
protected:
	qint64 t1;
	qint64 t2;
	bool stopped;
};

#endif // CRONOMETRE_H
